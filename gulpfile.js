var gulp = require('gulp');
const { series, parallel, watch } = require('gulp');
var sass = require('gulp-sass')(require('sass'));
var browserSync = require('browser-sync').create();
var header = require('gulp-header');
var cleanCSS = require('gulp-clean-css');
var rename = require("gulp-rename");
var uglify = require('gulp-uglify-es').default;
var concat = require('gulp-concat');
var pkg = require('./package.json');
var pump = require('pump');

// Set the banner content
var banner = ['/*!\n',
  ' * <%= pkg.title %> v<%= pkg.version %> \n',
  ' */\n',
  ''
].join('');

// Compiles SCSS files from /scss into /css
// gulp.task('sassStylesheets', function() {
//   pump([
//     gulp.src('app/scss/*.scss'),
//     sass(),
//     header(banner, {
//       pkg: pkg
//     }),
//     gulp.dest('app/css'),
//     browserSync.reload({
//       stream: true
//     })
//   ])
// });

// Minify compiled CSS
// ['sass']
function styles(cb) {
  //copy vendor files and fonts
  gulp.src(['app/scss/**/*', '!app/scss/*.scss'])
    .pipe(gulp.dest('app/output/css'))
    .pipe(browserSync.reload({
      stream: true
    }));

  //transform, and minify scss files
  gulp.src(['app/scss/*.scss'])
    .pipe(sass())
    .pipe(cleanCSS())
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest('app/output/css'))
    .pipe(browserSync.reload({
      stream: true
    }));

  cb();
}

function javascript(cb) {
  var options = {};
  gulp.src(['app/js/*.js', '!app/js/**/*.min.js'])
    .pipe(uglify(options))
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest('app/output/js'))
    .pipe(browserSync.reload({
      stream: true
    }));
  gulp.src(['app/js/**/*.min.js'])
    .pipe(gulp.dest('app/output/js'))
    .pipe(browserSync.reload({
      stream: true
    }));
  cb();
}

function outputAssets(cb) {
  gulp.src(['app/*.html'])
    .pipe(gulp.dest('app/output'))
    .pipe(gulp.src(['app/images/*']))
    .pipe(gulp.dest('app/output/images'))
    .pipe(browserSync.reload({
      stream: true
    }));

  // gulp.src(['app/css/**/*.woff2'])
  //   .pipe(gulp.dest('app/output/css'))
  //   .pipe(browserSync.reload({
  //     stream: true
  //   }));

  copyVendorFiles();
  cb();
}

function copyVendorFiles(cb) {
  // gulp.src([
  //     'node_modules/bootstrap/dist/**/*',
  //     '!**/npm.js',
  //     '!**/bootstrap-theme.*',
  //     '!**/*.map'
  //   ])
  //   .pipe(gulp.dest('app/output/vendor/bootstrap'))

  // gulp.src(['node_modules/jquery/dist/jquery.js', 'node_modules/jquery/dist/jquery.min.js'])
  //   .pipe(gulp.dest('app/output/js/vendor/jquery'))

  // gulp.src(['node_modules/jquery.easing/*.js'])
  //   .pipe(gulp.dest('app/output/js/vendor/jquery-easing'))

  // gulp.src([
  //     'node_modules/font-awesome/**',
  //     '!node_modules/font-awesome/**/*.map',
  //     '!node_modules/font-awesome/.npmignore',
  //     '!node_modules/font-awesome/*.txt',
  //     '!node_modules/font-awesome/*.md',
  //     '!node_modules/font-awesome/*.json'
  //   ])
  //   .pipe(gulp.dest('app/output/css/vendor/font-awesome'));
}

// // Default task
// // gulp.task('default', ['sass', 'minify-css', 'minify-js', 'copy', 'concat-css']);
// gulp.task('default', ['sass', 'minify-css', 'minify-js']);

function browser(cb) {
  browserSync.init({
    server: {
      baseDir: 'app/output',
      https: {
        key: "certs/testing.key",
        cert: "certs/testing.crt"
      }
    }
  });
  cb();
}

function browserWatch(cb){
  watch('app/scss/*.scss', styles);
  watch(['app/js/**/*.js', '!app/js/**/*.min.js'], javascript);
  // gulp.watch('app/css/*.css', ['minify-css', 'concat-css']);
  // Reloads the browser whenever HTML or JS files change
  watch('app/*.html', outputAssets);
  cb();
}

exports.build = parallel(styles, javascript, outputAssets);
exports.serve = series(exports.build, 
                  parallel(browser, browserWatch));
exports.default = browserWatch;